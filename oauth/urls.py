from django.urls import re_path, path
from .views import *
from dj_rest_auth.views import LoginView

urlpatterns = [
    re_path(r'^google/?$', GoogleLogin.as_view(), name='login'),
    re_path(r'^verify-token/?$', CustomVerifyToken.as_view(), name='token_verify'),
    path('register/', RegisterView.as_view()),
    path('login/', LoginView.as_view()),
]
