from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from uuid import uuid4

# Create your models here.


class User(AbstractUser):
    id = models.CharField(max_length=255, default=uuid4,
                          primary_key=True, editable=False)
