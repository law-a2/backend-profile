from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from dj_rest_auth.registration.views import SocialLoginView
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import GenericAPIView
from .models import User
from .serializers import CustomUserModelSerializer, RegisterSerializer
from rest_framework import status
from rest_framework_simplejwt.authentication import JWTAuthentication


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class RegisterView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        email = request.data['email']
        username = email.split('@')[0]
        password = request.data['password']
        first_name = request.data['first_name']
        last_name = request.data['last_name']

        if User.objects.filter(email__iexact=email).exists():
            return Response({"message": "Email sudah ada"}, status=status.HTTP_409_CONFLICT)

        user = User(
            email=email, username=username, first_name=first_name, last_name=last_name
        )
        user.set_password(password)

        user.save()

        # auto login after register
        loginResponse = requests.post(request.build_absolute_uri('/api/v1/auth/login/'), data={
            'email': email,
            'password': password
        })

        return Response(loginResponse.json(), status=loginResponse.status_code)


class CustomVerifyToken(APIView):
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        JWT_authenticator = JWTAuthentication()

        # authenitcate() verifies and decode the token
        # if token is invalid, it raises an exception and returns 401
        response = JWT_authenticator.authenticate(request)
        if response is not None:
            # unpacking
            user, token = response
            return Response(CustomUserModelSerializer(user).data, status=status.HTTP_200_OK)
        else:
            print("no token is provided in the header or the header is missing")
