from rest_framework.serializers import ModelSerializer
from oauth.models import User
from dj_rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers


class CustomUserModelSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'is_superuser']

class RegisterSerializer(ModelSerializer):
    password = serializers.CharField(write_only=True, style={'input_type': 'password'})
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']